#include <stdio.h>
#include <iostream>
#include <fstream>
#include <vector>
#include <typeinfo>

int main(){
  std::ifstream ifile("dog.bin");
  if(!ifile){
    std::vector<int> vec {1,2,3,4};
    size_t s = vec.size();
    std::ofstream o("dog.bin", std::ios::binary);
    o.write((char *) &s, sizeof(size_t));
    o.write((char *) vec.data(), sizeof(int) * s);
    o.close();
  }
  std::ifstream i("dog.bin", std::ios::binary);
  size_t s;
  i.read((char *) &s, sizeof(s));
  std::cout <<"size: " << s << std::endl;
  std::vector<int> vec2(s);
  std::cout << (typeid(size_t) == typeid(vec2.size()) ? "true" : "false") << std::endl;
  i.read((char *) vec2.data(), sizeof(int) * s);
  i.close();
  for(auto& e  : vec2){
    std::cout << e << " ";
  }
  exit(0);
}
