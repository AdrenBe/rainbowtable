#include <cstdlib>
#include <iostream>
#include <openssl/sha.h>
#include <string.h>
#include <stdio.h>
#include <vector>
#include <fstream>
#include <cstring>

u_int8_t* hash(char *buf){
  SHA256_CTX ctx;
  u_int8_t *results = new u_int8_t [SHA256_DIGEST_LENGTH];
  int n = strlen(buf);
  SHA256_Init(&ctx);
  SHA256_Update(&ctx, (u_int8_t *)buf, n);
  SHA256_Final(results, &ctx);
  return results;
}

int main(){  
  std::ifstream ifile("hash.bin");
  if(!ifile){
    char *buf;
    buf = (char *)"aaaabbbb";
    unsigned char  *results = hash(buf);
    /* Print the digest as one long hex value */
    //printf("0x");
    for (int n = 0; n < SHA256_DIGEST_LENGTH; n++){
       printf("%02x", results[n]);
    }
    std::cout << std::endl;
    //for (int n = 0; n < 8; n++){
    //   printf("%02x", buf[n]);
    //}
    //putchar(buf[8]=='\0' ? 'a' : 'b');
    unsigned char *buffer = new unsigned char[8];
    std::memcpy(buffer,buf,9);
    printf("%p\n", buffer);
    printf("%p\n", results);
    std::vector<unsigned char *>vec {buffer,results};
    //vec.push_back(buffer);
    //vec.push_back(results);
    size_t s = vec.size();
    // printf("%p\n", buffer);
    //printf("%p\n", results);
    for(auto a : vec){
      printf("%s\n",a);
    }
    std::ofstream o ("hash.bin", std::ios::binary);
    o.write((char *) &s, sizeof(size_t));
    int i= 1;
    for(auto a: vec){
      if(i%2){
	o.write((char *)a,8);
      }
      else
      {
	o.write((char *)a,32);
      }
      i++;
    }
    o.close();
  }else{
    std::ifstream i("hash.bin", std::ios::binary);
    size_t s;
    i.read((char *) &s, sizeof(s));
    std::vector <unsigned char *> vec2 (s);
    int j=1;
    for(auto &a:vec2){
      if(j%2){
	a = new unsigned char [8];
	printf("%p\n",a);
	i.read((char *)a,8);
	printf("%s\n",a);
      }else{
	a = new unsigned char [32];
	printf("%p\n",a);
	i.read((char *)a,32);
	printf("%s\n",a);
      }
    }
    i.close();
    for(auto e : vec2){
      printf("%s\n",e);
    }
    std::cout << std::endl;
  }
  exit(0);
}
