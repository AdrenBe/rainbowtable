#include <iostream>
#include "sha256.h"
#include <vector>
#include <fstream>
#include <cmath>
 
using std::string;
using std::cout;
using std::endl;

//recursive function that will generate a list of plaintext passwords
std::vector<string> getVector(int depthRequired, int depthCompleted, std::vector<string> tempResult){
	std::vector<string> result = tempResult;
	
	depthCompleted++;
	
	//again, lowercase and numbers have to be added
	std::vector<string> alphabet{
    "A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", 
    "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z",
    "a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m",
    "n", "o", "p", "q", "r", "s", "t", "u", "v", "w", "x", "y", "z",
    "1", "2", "3", "4", "5", "6", "7", "8", "9", "0"};	
	
	if(depthCompleted < depthRequired){
		
		for (int i = 0; i < tempResult.size(); i++){
			for (int j = 0; j < alphabet.size(); j++){
				if(result[i].length() == depthCompleted){
					result.push_back(result[i] + alphabet[j]); //this is where it gets added to the vector
				}				
			}
		}
		
		return getVector(depthRequired, depthCompleted, result);
	}	
	return result; // will only return result when it has achieved the depth required (password length required)
	
	
}

// This method will hold the logic for hashing multiple times and reduction function
string calculation(string toBeCalculated, std::vector<string> alphabet){
    
    string plaintext = toBeCalculated;
    string result = "";
    
    for(int j = 0; j < 5000000; j++){
        string hashed = sha256(plaintext);
            std::hash<std::string> hash_fn;
            int reduced = abs((hash_fn(hashed)));
            //int newReduced = (reduced + j) % (36^8);
            string reducedString = std::to_string(reduced);
            result = "";
            if (reducedString.length() < 9git  {
                reducedString += "000000000";
            }
        for(int i = 0; i < 8; i++){           
            string pointer = "";
            pointer += reducedString.at(i);        
            pointer += reducedString.at(i+1);   
            //std::cout << pointer << " points to " << (alphabet[(std::stoi(pointer))%62]) << std::endl;
            result += (alphabet[(std::stoi(pointer))%62]);
            //cout << "result is " << result << std::endl;
        }   
        plaintext = result;
        cout << result << std::endl;
    }
    
	return result;
}


//gets a list of plaintext passwords and puts them in a text file with their respective hashes.
int printHashes(int length){	

	// this is the alphabet used to generating the plaintexts, lowercase and numbers still have to be added
	std::vector<string> alphabet{
    "A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", 
    "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z",
    "a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m",
    "n", "o", "p", "q", "r", "s", "t", "u", "v", "w", "x", "y", "z",
    "1", "2", "3", "4", "5", "6", "7", "8", "9", "0"}; //length 62
    
	//this gets a list of all possible plaintext passwords
	/*std::vector<string> data = getVector(length,0, alphabet);	
	
	std::ofstream text;
	text.open("test.txt");
	
	// this for loop prints all the plaintexts and result hashes into a text file
	for (int i = 0; i < data.size(); i++){
		text << (data[i]) << ":" << (calculation(data[i])) << std::endl;
	}	
	
	text.close();*/
	
    calculation("kjfdng", alphabet);
    
	return 0;
}
 
 
 //This method calls everything
int main(int argc, char *argv[])
{
    printHashes(3); //Fill in how long the plaintext passwords can be
	
	cout << "done";
	
    return 0;
}


