#include <iostream>
#include <string>
#include <vector>
#include <sstream>
#include <openssl/sha.h>
#include <stdio.h>
#include <ctype.h>
#include <math.h>
#include <pthread.h>
#include <ctime>
#include <iomanip>
#include <fstream>
#include <cstring>
#include <mutex>
#include <typeinfo>

std::mutex mtx;
// Here we can modify the line of the tables, col, and threads.
#define NUM_THREADS 4
#define NLINE 1000000
#define NCOL 50000
const std::string filename ="hash.bin";
const size_t head_len = 8;

const size_t tail_len = SHA256_DIGEST_LENGTH;

int linedone = 0;
typedef u_int8_t u8;

struct data {
  unsigned char *head;
  unsigned char *tail;
} rdata;

void print_hex(const u8 *buf, size_t len){
  for(size_t i=0; i<len; i++){
    printf("%02hhx", buf[i]);
  }
}


/// decodes a hex string to u8s
uint8_t* hex_decode(char *in, size_t len, uint8_t *out)
{
    unsigned int i, t, hn, ln;

    for (t = 0,i = 0; i < len; i+=2,++t) {

            hn = in[i] > '9' ? (in[i]|32) - 'a' + 10 : in[i] - '0';
            ln = in[i+1] > '9' ? (in[i+1]|32) - 'a' + 10 : in[i+1] - '0';

            out[t] = (hn << 4 ) | ln;
    }
    return out;
}

/// write the total number of head-tail pairs in the first (sizeof size_t) bytes of the file
void writesizetofile(size_t s){
  std::ofstream o (filename,std::ios::binary | std::ios_base::app);
  o.write((char *)&s, sizeof(size_t));
}
/* write a head/tail pair in the file
 * Threadsafe but risk of deadlock
 * FIXME: only .close() on last writetofile
 * FIXME_opt: try-catch block with some error recovery
 */
int write_cnt = 0, write_percent = 0;
void writetofile(char * head, u_int8_t * tail){
  mtx.lock();
  std::ofstream o (filename,std::ios::binary | std::ios_base::app);
  o.write((char *) head, head_len);
  o.write((char *) tail, tail_len);
  //if NLINE < 100 then modulo 0 = UB thu 1 + NLINE/100
  linedone++;
  if(write_cnt++ % (1 + NLINE/100) == 0)
    std::cout << "\r" << write_percent++ << "%" << std::flush;
  if(linedone>=NLINE+NUM_THREADS)
    o.close();
  mtx.unlock();
}

u_int8_t* sha256(char *buf){
  SHA256_CTX ctx;
  u8 *results = new u8 [tail_len];
  int n = strlen(buf);
  SHA256_Init(&ctx);
  SHA256_Update(&ctx, (u_int8_t *)buf, n);
  SHA256_Final(results, &ctx);
  return results;
}
//Inspired of : https://github.com/JasonPap/Rainbow-Table-Generator/blob/master/RT-Generator/RainbowTable.cpp
void reductionfunction(int n, u_int8_t* HashValue, char *PreHashValue){
    const char charset[] =
        "0123456789"
        "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
        "abcdefghijklmnopqrstuvwxyz"
        "!@";
    /*std::cout << "Hash Value = ";
    for (int n = 0; n <tail_len; n++){
       printf("%02x", HashValue[n]);
       }
       std::cout << std::endl;*/
    for(int i = 0; i < head_len; i++)
    {
        unsigned int index;
        index = HashValue[i] + HashValue[i+8] + HashValue[i+16] + HashValue[i+24] + n;
        PreHashValue[i] =charset[index%64];
    }
}
void gen_random(char *s, const int len) {
    static const char alphanum[] =
        "0123456789"
        "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
        "abcdefghijklmnopqrstuvwxyz"
        "!@";
    /* FIXME(ngyj) : rand() for each fucking byte is _slow_
     *    if you have time use `mt19937_64` : it's 64bits => 8 bytes
     *    interpret it as 8 `chars` and you have your 8 random chars for password
     */
    for (int i = 0; i < len; i++) {
        s[i] = alphanum[rand() % (sizeof(alphanum) - 1)];
    }
}

void *doit(void *threadid){
  do{
    char head[head_len+1];
    gen_random(head,head_len);
    char ephem [head_len+1];
    head[head_len] = '\0'; 
    strcpy(ephem,head);
    u8 * tail;
    //std::cout << " Head = " << head << std::endl;
    for(int i=1;i<=NCOL;i++){
      tail = sha256(ephem);
      reductionfunction(i,tail,ephem);
      delete tail;
      //std::cout << " Reducted hash = " << ephem << std::endl;
    }
    tail = sha256(ephem);
    //std::cout << " final tail = ";
    //  print_hex(tail,SHA256_DIGEST_LENGTH);
    writetofile(head,tail);
    delete tail;
    //std::cout << std::endl;
  }while(linedone<NLINE);
  pthread_exit(NULL);
}
bool hashcmp(u8 * a, u8 * b){
  for(int i=0; i < tail_len; i++){
    if(a[i] != b[i])
      return false;
  }
  return true;
}

int main(){
  std::ifstream ifile(filename);
  if(!ifile){
   writesizetofile(NLINE);
   srand(time(NULL));
   pthread_t threads[NUM_THREADS];
   for( int i = 0; i < NUM_THREADS; i++ ) {
     int rc = pthread_create(&threads[i], NULL, doit, (void*)i);
      if (rc) {
          std::cout << "Error:unable to create thread," << rc << std::endl;
         exit(-1);
      }
   }
   pthread_exit(NULL);
  }else{
    std::ifstream i("hash.bin", std::ios::binary);
    size_t s;
    i.read((char *) &s, sizeof(s));
    std::cout << "Size of vector = " << s << std::endl;
    std::vector <struct data> vec(s);
    std::cout << "Printing table.. " << std::endl;
    for(auto &a : vec){
        auto buf = new u8 [head_len + tail_len + 1];
        i.read((char *) buf, head_len);
        buf[head_len] = '\0';
        i.read((char *) (buf + head_len + 1), tail_len);
        a.head = buf;
        a.tail = buf + head_len + 1;
	printf("Head ====> %s\n",a.head);
	print_hex(a.tail,tail_len);
	printf("\n");
    }
    printf("\n");
    std::cout << "Give hash = ";
    char charhash[65];
    std::cin.get(charhash,65);
    charhash[65]='\0';
    u8 * hash = new u8[tail_len];
    hex_decode(charhash,tail_len*2,hash);
    //hash[tail_len]='\0';
    //std::cout <<"Give hash = ";
    //print_hex(hash,tail_len);
    printf("\n");
    bool found = false;
    char ephem[head_len+1];
    ephem[head_len+1] = '\0';
    int nbtries = 0;
    int foo=1;
    // std::cout << "VECSIZE "<<vec.size() << std::endl;
    while(!found && nbtries<=NCOL){
      for(auto &a:vec){
	//print_hex(hash,tail_len);
	//printf("\n");
	if(hashcmp(a.tail,hash)){
	  std::cout << "Ok" <<std::endl;
	  auto hd = (char *)a.head;
	  //std::cout << a.head <<std::endl;
	  // std::cout << "FOO = " << foo <<std::endl;
	  if(vec.size()==foo){
	    //std::cout << "HELLO" <<std::endl;
	    nbtries++;
	  }
	  for(int x=1;x<=NCOL-nbtries;x++){
	    hash=sha256(hd);
	    reductionfunction(x,hash,hd);
	    delete hash;
	  }
	  std::cout << "Le password est : " << hd;
	  found = true;
	  return 0;
	}
	 hex_decode(charhash,tail_len*2,hash);
	if(!found){
	 
	  for(int x =NCOL-nbtries;x<=NCOL;x++){
	    // std::cout << "X = " << x <<std::endl;
	    // std::cout << "ephem : " << ephem <<std::endl;
	    //print_hex(hash,tail_len);
	    reductionfunction(x,hash,ephem);
	     delete hash;
	    // std::cout << "ephem ==> " << ephem <<std::endl;
	    hash = sha256(ephem);
	    
	    //print_hex(hash,tail_len);
	    //printf("\n\n");
	     
	  }
	  
	  
	}
        foo++;
      }
      //delete hash;
      foo=0;
      nbtries++;
      std::cout << nbtries << "/" <<NCOL <<std::endl;
    }
		    
    i.close();
    
    exit(0);
  }
}
